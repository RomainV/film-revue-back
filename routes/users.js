var express = require('express');
var router = express.Router();

const UserController = require('../controller/userController');

/* User API. */
router.get('/', UserController.all);
router.post('/', UserController.create);
router.delete('/:_id', UserController.delete);
router.get('/:_id/reviews', UserController.getReviews);

module.exports = router;
