var express = require('express');
const MongoClient = require("mongodb");
var router = express.Router();
const ReviewController = require('../controller/reviewsController')

/* Reviews API. */
router.post('/', ReviewController.create);

module.exports = router;
