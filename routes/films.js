var express = require('express');
const MongoClient = require("mongodb");
var router = express.Router();
const FilmController = require('../controller/filmsController')

/* Films API. */
router.get('/', FilmController.all);
router.post('/', FilmController.create);
router.get('/:_id/reviews', FilmController.getReviews);

module.exports = router;
