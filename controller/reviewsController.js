
const Review = require( '../model/review' );
const Film = require('../model/film');
const User = require('../model/user')

let ReviewController = {
    create: async (req, res) => {
        new Review(req.body).save((error, document) => {
            if (error)
                res.send(error)
            Film.findOne({_id: req.body.reference}).then( (doc) => {
                doc.reviews.push(document._id);
                doc.save();
            });
            User.findOne({_id: req.body.owner}).then( (doc) => {
                doc.reviews.push(document._id);
                doc.save();
            });
            res.send(document);
        });
    },

}

module.exports = ReviewController
