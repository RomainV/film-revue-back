
const Film = require( '../model/film' );

let FilmController = {
    all:async (req, res) => {
        Film.find({}, (err, docs) => {
            res.send(docs);
        });
    },

    create: async (req, res) => {
        req.body.creation_date = Date.now();
        new Film(req.body).save((error, document) => {
            if (error)
                res.send(error)
            res.send(document);
        });
    },

    getReviews: async (req, res) => {
        Film.findOne({_id: req.params._id}).populate({
            path: 'reviews',
            populate: {
                path: 'owner',
                select: 'pseudo',
            }
        }).then( (doc) => {
            res.send(doc);
        })
    }
}

module.exports = FilmController
