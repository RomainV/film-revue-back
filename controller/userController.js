
const User = require( '../model/user' );

let UserController = {
    all:async (req, res) => {
        User.find({}, (err, docs) => {
            res.send(docs);
        });
    },

    create: async (req, res) => {
        new User({
            pseudo: req.body.pseudo
        }).save((error, document) => {
            if (error)
                res.send(error)
            res.send(document);
        });
    },

    delete: async (req, res) => {
        User.deleteOne({_id: req.params._id}).then( () => {
            res.send();
        });
    },

    getReviews: async (req, res) => {
        User.find({_id: req.params._id}).populate('reviews').then( (err, doc) => {
            res.send(doc);
        })
    }
}

module.exports = UserController
