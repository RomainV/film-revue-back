const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema({
    pseudo: String,
    reviews: [{
        type: Schema.Types.ObjectId,
        ref: "Review"
    }]
})

module.exports = mongoose.model('User', userSchema);
