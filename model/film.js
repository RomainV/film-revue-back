const mongoose = require('mongoose')
const Schema = mongoose.Schema

const filmSchema = new Schema({
    title: String,
    tmdb_id: String,
    reviews: [{
        type: Schema.Types.ObjectId,
        ref: "Review"
    }],
    creation_date: Date
})

module.exports = mongoose.model('Film', filmSchema);
