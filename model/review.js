const mongoose = require('mongoose')
const Schema = mongoose.Schema

const reviewSchema = new Schema({
    title: String,
    text: String,
    note: Number,
    publicationDate: Date,
    owner: {
        type: Schema.Types.ObjectId,
        ref: "User",
    },
    reference: {
        type: Schema.Types.ObjectId,
        ref: "Film",
    }
})

module.exports = mongoose.model('Review', reviewSchema);
